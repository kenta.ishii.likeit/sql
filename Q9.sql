SELECT
t1.category_name,
SUM(t.item_price) AS total_price
FROM
item t
LEFT OUTER JOIN
item_category t1
ON
t.`category_id`=t1.`category_id`
GROUP BY
t1.`category_name`;
